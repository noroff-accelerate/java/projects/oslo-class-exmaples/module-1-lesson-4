# Zoo keeper

Very basic demonstration of OOP. There is an Animal super class, some categories of animals, and finally the actual animal subclasses. 

An interface is used to demonstrate attaching behaviour to classes. 