package main.java.animals.fish;

public class Torsken extends Fish {
    @Override
    public String makeNoise() {
        return "Torsken kommer nå";
    }

    @Override
    public String getInfo() {
        return "Prime currency of Norges Bank";
    }

    @Override
    public String swim() {
        return "Swim into the ATM";
    }
}
