package main.java.animals.fish;

import main.java.animals.Animal;

public abstract class Fish extends Animal {
    public abstract String swim();
}
