package main.java.animals;

public abstract class Animal {
    public abstract String makeNoise();

    public String getInfo(){
        return "I am an animal!";
    }
}
