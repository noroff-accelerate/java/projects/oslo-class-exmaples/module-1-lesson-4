package main.java.animals.mammal;

public class Bear extends Mammal {
    @Override
    public String makeNoise() {
        return "Rawr";
    }

    @Override
    public String getInfo() {
        return "Im not a furry";
    }

    @Override
    public String run() {
        return "No it doesnt";
    }
}
