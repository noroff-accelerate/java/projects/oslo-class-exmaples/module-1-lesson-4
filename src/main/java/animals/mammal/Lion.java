package main.java.animals.mammal;

import main.java.behaviours.Biter;

public class Lion extends Mammal implements Biter {
    @Override
    public String makeNoise() {
        return "Roar";
    }

    @Override
    public String getInfo() {
        return "Testosterone";
    }

    @Override
    public String run() {
        return "You should run";
    }

    @Override
    public String bite() {
        return "Nomnom";
    }

    @Override
    public String cleanTeeth() {
        return "Off to the dentist";
    }
}
