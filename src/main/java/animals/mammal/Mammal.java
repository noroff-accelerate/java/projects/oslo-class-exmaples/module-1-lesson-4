package main.java.animals.mammal;

import main.java.animals.Animal;

public abstract class Mammal extends Animal {
    public abstract String run();
}
