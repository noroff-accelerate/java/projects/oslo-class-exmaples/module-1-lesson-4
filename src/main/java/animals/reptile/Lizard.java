package main.java.animals.reptile;

public class Lizard extends Reptile {
    @Override
    public String makeNoise() {
        return "Silence";
    }

    @Override
    public String getInfo() {
        return "Needs lotion";
    }

    @Override
    public String sunbath() {
        return "Oh yes";
    }
}
