package main.java.animals.reptile;

import main.java.behaviours.Biter;

public class Snake extends Reptile implements Biter {
    @Override
    public String makeNoise() {
        return "Hiss";
    }

    @Override
    public String getInfo() {
        return "Why do you exist";
    }

    @Override
    public String sunbath() {
        return "Spicy danger noodle";
    }

    @Override
    public String bite() {
        return "Venom";
    }

    @Override
    public String cleanTeeth() {
        return "They fall out";
    }
}
