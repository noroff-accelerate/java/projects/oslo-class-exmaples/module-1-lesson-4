package main.java.animals.reptile;

import main.java.animals.Animal;

public abstract class Reptile extends Animal {
    public abstract String sunbath();
}
