package main.java;

import main.java.animals.Animal;
import main.java.animals.fish.Dolphin;
import main.java.animals.fish.Fish;
import main.java.animals.fish.Torsken;
import main.java.animals.mammal.Bear;
import main.java.animals.mammal.Lion;
import main.java.animals.mammal.Mammal;
import main.java.animals.reptile.Lizard;
import main.java.animals.reptile.Reptile;
import main.java.animals.reptile.Snake;
import main.java.behaviours.Biter;

import java.util.ArrayList;

public class Program {
    public static void main(String[] args) {
        // Create one of each animal
        Snake snek = new Snake();
        Torsken twoHundy = new Torsken();
        Lion notSimba = new Lion();
        Dolphin dolly = new Dolphin();
        Bear putin = new Bear();
        Lizard lizzy = new Lizard();
        // List of all animals called zoo
        ArrayList<Animal> zoo = new ArrayList<>();
        zoo.add(snek);
        zoo.add(twoHundy);
        zoo.add(notSimba);
        zoo.add(dolly);
        zoo.add(putin);
        zoo.add(lizzy);

        // Demonstrate all of them making noise and getting info
        for (Animal a: zoo) {
            System.out.println(a.makeNoise());
        }

        // Make a list of each category of animals (mammal, reptile, fish)
        ArrayList<Reptile> scalyLads = new ArrayList<>();
        scalyLads.add(snek);
        scalyLads.add(lizzy);
        ArrayList<Mammal> memels = new ArrayList<>();
        memels.add(notSimba);
        memels.add(putin);
        ArrayList<Fish> fesh = new ArrayList<>();
        fesh.add(twoHundy);
        fesh.add(dolly);

        // Demonstrate their extended behaviour
        for (Reptile r: scalyLads) {
            System.out.println(r.sunbath());
        }
        for (Mammal m: memels) {
            System.out.println(m.run());
        }
        for (Fish f: fesh) {
            System.out.println(f.swim());
        }


        ArrayList<Biter> chompers = new ArrayList<>();
        chompers.add(notSimba);
        chompers.add(snek);

        for (Biter b: chompers) {
            System.out.println(b.bite());
            System.out.println(b.cleanTeeth());
        }
    }
}
