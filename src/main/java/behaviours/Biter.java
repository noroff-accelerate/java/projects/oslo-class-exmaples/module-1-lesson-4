package main.java.behaviours;

public interface Biter {
    String bite();
    String cleanTeeth();
}
